<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('master.master')->nest('child', 'home');
    }

    public function libur()
    {
        $thn=date('Y');
        $url = env('API_BASE_URL')."/ref/hari-libur/".$thn;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;

            return json_encode($data['data']);
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }

    }

     public function list_pegawai()
    {
        $thn=date('Y');
        $url = env('API_BASE_URL')."/ref/list-pegawai/".Session('unitKerja');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;

            return json_encode($data['data']);
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);  
        }

    }
}
