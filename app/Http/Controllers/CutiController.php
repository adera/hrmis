<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;
use DateTime;
use File;
class CutiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
         public function __construct()
    {
        $this->middleware('auth');
    }    

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bln=date('m');
        $thn=date('Y');

         if($request->get('bln')){
            $bln=$request->get('bln');
        }
        if($request->get('thn')){
            $thn=$request->get('thn');
        }

        $url = env('API_BASE_URL')."/cuti?limit=100&bln=".$bln."&thn=".$thn;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';
        }


        $param['data']=$data;

        $param['bln']=$bln;
        $param['thn']=$thn;

        return view('master.master')->nest('child', 'cuti.index',$param);
    }
    public function add_cuti(Request $request){
        $url = env('API_BASE_URL')."/absen/approval/list-usr-kontigensi";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';           
        }

        $url1 = env('API_BASE_URL')."/cuti/jenis";
        $client1 = new Client();
        $headers1 = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result1 = $client1->get($url1,[
                RequestOptions::HEADERS => $headers1,
                'verify'=>false
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $jns1 = json_decode($param2, true);
            $jns =$jns1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $jns='';           
        }

        $param['data']=$data;
        $param['jns']=$jns;

        return view('master.master')->nest('child', 'cuti.add_cuti',$param);
    }
    public function insert_cuti(Request $request){

        $url = env('API_BASE_URL')."/cuti/";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        if($request->get('ambil_cuti')=='true'){
            $a=true;
        }else{
            $a=false;
        }
        $data = array(

            'jenis_cuti_id'=> (int) $request->input('idjns'),
            'tgl_mulai'=> date('Y-m-d',strtotime($request->input('start'))),
            'tgl_selesai'=> date('Y-m-d',strtotime($request->input('end'))),
            'user_id_rekomendasi'=> (int) $request->input('user_approval1'),
            'user_id_approval'=> (int) $request->input('user_approval'),
            'tgl_pengajuan'=> date('Y-m-d'),
            'note_alasan'=> $request->input('alasan'),
            'jatah_cuti'=>0,
            'tahun'=> (int) $request->input('tahun'),
            'bekal_cuti'=>(bool) $a
        );       
        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }

    }
    public function hapus_cuti(Request $request){

        $url = env('API_BASE_URL')."/cuti/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->delete($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            return $data1;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return $response;
        }

    }
}

