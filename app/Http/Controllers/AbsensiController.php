<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;
use DateTime;
use File;
class AbsensiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
        

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bln=date('m');
        $thn=date('Y');

        if($request->get('bln')){
            $bln=$request->get('bln');
        }
        if($request->get('thn')){
            $thn=$request->get('thn');
        }
       
        $url = env('API_BASE_URL')."/absen?limit=100&bln=".$bln."&thn=".$thn;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                'verify'=>false
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }


        $param['data']=$data;
        $param['bln']=$bln;
        $param['thn']=$thn;
        return view('master.master')->nest('child', 'absensi.index',$param);
    }
    public function add_kontigensi(Request $request)
    {
        $url = env('API_BASE_URL')."/absen/approval/list-usr-kontigensi";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            
        
        }


        $param['data']=$data;
        $param['id']=$request->get('id');
        $param['tgl']=$request->get('tgl');
        $param['jamMasuk']=$request->get('jamMasuk');
        $param['jamKeluar']=$request->get('jamKeluar');
        return view('master.master')->nest('child', 'absensi.add_kontigensi',$param);
    }
    public function insert_kontigensi(Request $request){


        $url = env('API_BASE_URL')."/absen/kontigensi";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $data = array(
            'id_absen'=> (int) $request->input('id'),
            'jam_masuk'=> date('Y-m-d',strtotime($request->input('tgl'))).' '.date('H:i:s',strtotime($request->input('jam_masuk'))),
            'jam_keluar'=> date('Y-m-d',strtotime($request->input('tgl'))).' '.date('H:i:s',strtotime($request->input('jam_keluar'))),
            'alasan'=> $request->input('alasan'),
            'user_id_approval'=> (int) $request->input('user_approval')
        );

        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            

            //dd($data1);                

            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd(json_encode($response));
            //dd($response);
            return json_encode($response);
        }

    }
}
