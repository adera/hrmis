<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;
use DateTime;
use File;

class UkesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
         public function __construct()
    {
        $this->middleware('auth');
    }    

     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $url = env('API_BASE_URL')."/pegawai/tanggungan/detail";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data='';
        }

        $url1 = env('API_BASE_URL')."/ukes/";
        $client1 = new Client();
        $headers1 = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result1 = $client1->get($url1,[
                RequestOptions::HEADERS => $headers1,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data2 = json_decode($param2, true);
            $data2 =$data2;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data2='';
        }


        $param['data_tanggungan']=$data;
        $param['data_history']=$data2;

        return view('master.master')->nest('child', 'ukes.index',$param);
    }

    public function form_ukes(Request $request){

        $url = env('API_BASE_URL')."/ref/jns-kesehatan";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1;
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
        }
        
        $param['data']=$data;
        return view('master.master')->nest('child', 'ukes.form',$param);


    }

    public function insert_ukes(Request $request){
        $id=$request->input('id');
        $tanggungan='';
        /*
        if($request->get('tanggungan')==0){
            $tanggungan=0;
        }else{
            $tanggungan=$request->input('nama_pasien');
        }
        */

        if($request->input('nama_pasien')){
            $tanggungan=$request->input('nama_pasien');
        }else{
            $tanggungan=4;
        }

       // dd($tanggungan);




        // $media = $request->file('file');
        if($request->hasFile('file')){
            $file = $request->file('file');

            // Mendapatkan Nama File
            $nama_file = $file->getClientOriginalName();
       
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();

            $file_mime = $file->getmimeType();
            $fileContent = File::get($file);
        }

        $url = env('API_BASE_URL')."/ukes/";
        $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => $nama_file,
                            'Mime-Type'=> $file_mime,
                            'extension'=> $extension
                        ],
                        [
                            'name'     => 'tanggungan_id',
                            'contents' => (int) $tanggungan,
                        ],
                        [
                            'name'     => 'nominal',
                            'contents' => (int) str_replace('.', '', $request->input('nominal')),
                        ],
                        [
                            'name'     => 'tgl_periksa',
                            'contents' => date('Y-m-d',strtotime($request->input('tgl'))),
                        ],
                        [
                            'name'     => 'nama_rs_dr',
                            'contents' => $request->input('nama_rs'),
                        ],
                        [
                            'name'     => 'jns_kesehatan_id',
                            'contents' => (int) $request->input('jns_kesehatan'),
                        ]
                        
                        
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

            return json_encode($data_result);
             
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return json_encode($response);
        }
    }
    public function gettanggungan(){

        $url = env('API_BASE_URL')."/pegawai/tanggungan";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];

        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            
            //return $this->form_lembur($data1);
            return json_encode($data1['data']);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //dd($response);
            return $response;
        }
    }
}


