<html>
<head>
  <title>BERITA ACARA</title>
</head>
<body>
  <style type="text/css">
    table tr td,
    table tr th{
      font-size: 9pt;
    }
    .table-td-th {
      border: 1px solid black;
    }
  </style>
 @php
  $peserta = \DB::select("select p.nmpegawai,j.nama from peserta_perdin pp
  left join pegawai p on p.nrp=pp.nrp
  left join jabatan j on j.id=p.idjab
  where perdin_id=".$module[0]->id."
  order by pp.id asc");
   @endphp
  <table width="100%">
    <tr>
      <td width="10" align="center"><img src="{{ public_path('img/logo.png')}}" style="width: 80px;height: 80px;"></td>
      <td align="center">
          <h3><u>BERITA ACARA</u></h3>
          <h4>NOMOR : {{$module[0]->nomor}}</h4> 
      </td>
      
      
    </tr>
    <tr>
      <td width="10" align="center">&nbsp;</td>
      <td align="center">
          <h3>TENTANG</h3>
          <h4>LAPORAN PELAKSANAAN LEMBUR</h4> 
      </td>
    </tr>
  </table>
  @php
  $peserta = \DB::select("select p.nmpegawai,al.*,jam_selesai::time - jam_mulai::time as durasi from anggota_lembur al
  left join pegawai p on p.nrp=al.nrp
  where lembur_id=".$module[0]->id."
  order by al.id asc");
   @endphp
  <table width="100%">
    <tr>
      <td><b>Pada hari ini telah tanggal {{date('d',strtotime($module[0]->tanggal))}} bulan {{date('m',strtotime($module[0]->tanggal))}} tahun {{date('Y',strtotime($module[0]->tanggal))}} telah dilaksanakan lembur dengan maksud dan tujuan :</b></td>
    </tr>
    <tr>
      <td>{{$module[0]->perihal}}</td>
    </tr>
    <tr>
      <td><b>Adapun rincian pekerjaan selama lembur tersebut adalah sebagai berikut :</b></td>
    </tr>
  </table>

  <table width="100%" class="table-td-th">
    <tr>
      <td class="table-td-th" rowspan="2">NO</td>
      <td class="table-td-th" rowspan="2">Nama Petugas</td>
      <td colspan="2" class="table-td-th">Waktu</td>
      <td class="table-td-th" rowspan="2">Jumlah Jam Lembur</td>
      <td class="table-td-th" rowspan="2">Rincian Pekerjaan Lembur</td>
      <td class="table-td-th" rowspan="2">Hasil Pekerjaan Lembur</td>
    </tr>
    <tr>
      <td class="table-td-th">Awal</td>
      <td class="table-td-th">Akhir</td>
    </tr>
    @php
    $no=0;
    @endphp
    @foreach($peserta as $item)
    @php
    $no++;
    @endphp
    <tr>
      <td class="table-td-th">{{$no}}</td>
      <td class="table-td-th">{{$item->nmpegawai}}</td>
      <td class="table-td-th" align="center">{{date('H:i',strtotime($item->jam_mulai))}}</td>
      <td class="table-td-th" align="center">{{date('H:i',strtotime($item->jam_selesai))}}</td>
      <td class="table-td-th" align="center">{{date('H:i',strtotime($item->durasi))}}</td>
      <td class="table-td-th">{{$module[0]->rincian_kerja}}</td>
      <td class="table-td-th">{{$module[0]->hasil_kerja}}</td>
    </tr>
    @endforeach
  </table>
  <table>
    <tr>
      <td>Demikian Berita Acara ini dibuat untuk dipergunakan sebagaimana mestinya .</td>
    </tr>
  </table>
 
</body>
</html>