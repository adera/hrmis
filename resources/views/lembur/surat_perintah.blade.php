<html>
<head>
  <title>SURAT TUGAS LEMBUR</title>
</head>
<body>
  <style type="text/css">
    table tr td,
    table tr th{
      font-size: 9pt;
    }
    .table-td-th {
      border: 1px solid black;
    }
  </style>
  
  @php
   $petugas = \DB::select("select p.nip,p.nmpegawai,j.nama,al.*,jam_selesai::time - jam_mulai::time as durasi from anggota_lembur al
    left join pegawai p on p.nrp=al.nrp
    left join jabatan j on j.id=p.idjab
    where lembur_id=".$module[0]->id." and jenis='petugas'
    order by al.id asc");
    $pengawas = \DB::select("select p.nip,p.nmpegawai,j.nama,al.*,jam_selesai::time - jam_mulai::time as durasi from anggota_lembur al
    left join pegawai p on p.nrp=al.nrp
    left join jabatan j on j.id=p.idjab
    where lembur_id=".$module[0]->id." and jenis='pengawas'
    order by al.id asc");
  @endphp
  <table width="100%">
    <tr>
      <td width="15" align="center"><img src="{{ public_path('img/logo.png')}}" style="width: 80px;height: 80px;"></td>
      <td>
          <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SURAT TUGAS LEMBUR</u></h3>
          <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOMOR : {{$module[0]->nomor}}</h4> 
      </td>
     </tr>
  </table>
  <table width="100%">
    <tr>
      <td></td>
      <td>1. Pejabat berwenang yang memberikan tugas lembur</td>
      <td>: &nbsp; {{$module[0]->nmpegawai}}</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>2. Nama & Jababtan pegawaiyg diberikan tugas lembur</td>
      <td>:</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td colspan="3">
        <table width="100%" class="table-td-th">
          <tr>
            <td class="table-td-th">No</td>
            <td class="table-td-th">NIP</td>
            <td class="table-td-th">Nama Pegawai</td>
            <td class="table-td-th">Jabatan</td>
            <td class="table-td-th">Sebagai</td>
            <td class="table-td-th">Mulai</td>
            <td class="table-td-th">Selesai</td>
            <td class="table-td-th">Lama</td>
          </tr>
          @php
          $no=0;
          @endphp
          @foreach($petugas as $item)
          @php
          $no++;
          @endphp
          <tr>
            <td class="table-td-th">{{$no}}</td>
            <td class="table-td-th">{{$item->nip}}</td>
            <td class="table-td-th">{{$item->nmpegawai}}</td>
            <td class="table-td-th">{{$item->nama}}</td>
            <td class="table-td-th">{{$item->jenis}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->jam_mulai))}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->jam_selesai))}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->durasi))}}</td>
          </tr>
          @endforeach
        </table>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>3. Nama Pengawas Lembur</td>
      <td>:</td>
      <td></td>
    </tr>
     <tr>
      <td></td>
      <td colspan="3">
        <table width="100%" class="table-td-th">
          <tr>
            <td class="table-td-th">No</td>
            <td class="table-td-th">NIP</td>
            <td class="table-td-th">Nama Pegawai</td>
            <td class="table-td-th">Jabatan</td>
            <td class="table-td-th">Sebagai</td>
            <td class="table-td-th">Mulai</td>
            <td class="table-td-th">Selesai</td>
            <td class="table-td-th">Lama</td>
          </tr>
          @php
          $no=0;
          @endphp
          @foreach($pengawas as $item)
          @php
          $no++;
          @endphp
          <tr>
            <td class="table-td-th">{{$no}}</td>
            <td class="table-td-th">{{$item->nip}}</td>
            <td class="table-td-th">{{$item->nmpegawai}}</td>
            <td class="table-td-th">{{$item->nama}}</td>
            <td class="table-td-th">{{$item->jenis}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->jam_mulai))}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->jam_selesai))}}</td>
            <td class="table-td-th">{{date('H:i',strtotime($item->durasi))}}</td>
          </tr>
          @endforeach
        </table>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>4. Hari/Tanggal Tugas Lembur Dilaksanakan</td>
      <td>: &nbsp; {{date('d M Y',strtotime($module[0]->tanggal))}} s.d {{date('d M Y',strtotime($module[0]->tanggal))}}</td>
      <td></td>
    </tr>
     <tr>
      <td></td>
      <td>5. Tempat Tugas Lembur Dilaksanakan</td>
      <td>: &nbsp; {{$module[0]->tempat}}</td>
      <td></td>
    </tr>
     <tr>
      <td></td>
      <td>6. Maksud Pelaksanaan Lembur</td>
      <td>: &nbsp; {{$module[0]->perihal}}</td>
      <td></td>
    </tr>
     <tr>
      <td></td>
      <td>7. Perkiraan Lamanya Pelaksanaan Tugas Lembur</td>
      <td>: &nbsp; {{date('H:i',strtotime($module[0]->durasi))}}</td>
      <td></td>
    </tr>
     <tr>
      <td></td>
      <td>8. Keterangan Lain-lain</td>
      <td>: &nbsp; {{$module[0]->perihal}}</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br><br><br><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">.......................,{{date('d M Y',strtotime($module[0]->tanggal))}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">bank bjb syariah</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><br><br></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">{{$module[0]->nmpegawai}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="text-align: center">( {{$module[0]->nama}} )</td>
    </tr>
  </table>
 
</body>
</html>