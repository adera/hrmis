@section('content')
<!--Begin::Section-->
 <style>
        .Holidays a{
            background-color: red !important; 
        }
</style>
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Lembur
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								<button type="button" onclick="loadNewPage('{{ route('add_lembur') }}')" class="btn btn-label-info btn-sm btn-upper">Tambah Lembur</button>
								<button type="button" onclick="loadNewPage('{{ route('lembur') }}')" class="btn btn-danger btn-sm btn-upper">Lembur Sebelumnya</button>
							</div>
						</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div id="dialog"></div>
							<div class="kt-portlet__body">
								<div class="form-group row">
									
									<label for="example-email-input" class="col-2 col-form-label">NIP</label>
									<div class="col-10">
										: &nbsp; {{Session('nip')}}
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Tanggal</label>
									<div class="col-10">
										: &nbsp; {{date('d M Y',strtotime($data_lembur['tgl_pengajuan']))}}
									</div>


									<label for="example-email-input" class="col-2 col-form-label">Perkiraan Jam</label>
									<div class="col-10">
										: &nbsp; {{date('H:i',strtotime($data_lembur['jam_mulai']))}}	s.d {{date('H:i',strtotime($data_lembur['jam_selesai']))}}
											
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Pemberi Tugas</label>
									<div class="col-10">
										: &nbsp; {{$data_lembur['approval']['nama']}}
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Maksud / Tujuan Lembur</label>
									<div class="col-10">
										: &nbsp; {{$data_lembur['perihal']}}
									</div>

									<label for="example-email-input" class="col-2 col-form-label">Tempat</label>
									<div class="col-10">
										: &nbsp; {{$data_lembur['tempat']}}
									</div>

								</div>
								<label for="example-email-input" class="col-2 col-form-label"><h5>Nomor :</h5></label>
								<label for="example-email-input" class="col-form-label"><h3>{{$data_lembur['nomor']}}</h3></label>	
							</div>
							<table class="table table-striped- table-hover" id="table_id" width="100%">
								<thead>
									<tr>
										<th title="Field #1">NIP</th>
										<th title="Field #2">Nama Pegawai</th>
										<th title="Field #1">Jabatan</th>
										<th title="Field #3">Sebagai</th>
										<th title="Field #4">Jam Mulai</th>
										<th title="Field #5">Jam Selesai</th>
										<th title="Field #5">Lama</th>
										<th title="Field #5">Aksi</th>
									</tr>
								</thead>
								<tbody>
									@if($data_peserta)
										@foreach($data_peserta as $item)
										<tr>
											<td>{{$item['nip']}}</td>
											<td>{{$item['nama']}}</td>
											<td>{{$item['jabatan']}}</td>
											<td>{{$item['jenis']}}</td>
											<td>{{date('H:i',strtotime($item['jam_mulai']))}}</td>
											<td>{{date('H:i',strtotime($item['jam_selesai']))}}</td>
											<td>
											@if($item['durasi'])
											-
											@else
											{{$item['durasi']['hours']}}
											@endif
											
											</td>
											<td>
												@if(Session('nip')==$item['nip'])
												@else
												@php
												$id_=$item['id'];
												@endphp
												<button type="button" class="btn btn-outline-hover-info btn-sm btn-icon btn-circle" onclick="edit_peserta('{{$id_}}');"><i class="fa fa-edit"></i></button>

												<button type="button" class="btn btn-outline-hover-danger btn-sm btn-icon btn-circle" onclick="hapus_peserta('{{$id_}}');"><i class="fa fa-trash"></i></button>
												@endif

												

											</td>
										</tr>
										@endforeach
									@endif
								</tbody>
							</table>
							<div class="kt-portlet__foot" style="text-align: right;">

								<button class="btn btn-info" onclick="print('{{$id}}',1);">Print Tugas Lembur</button>
								@if($data_lembur['rincian_kerja']=='')
								<button class="btn btn-info" onclick="berita_acara();">Print Berita Acara</button>
								@else
								<button class="btn btn-info" onclick="print('{{$id}}',2);">Print Berita Acara</button>
								@endif
								
								@if($data_lembur['is_print'])
								@else
								<button onclick="add_petugas()" class="btn btn-danger">Tambah Petugas</button>
								<button onclick="add_pengawas()" class="btn btn-danger">Tambah Pengawas</button>
								@endif
								
							</div>

							@if($data_lembur['rincian_kerja']=='')
								<div id="ba" style="display: none;">
									<hr>
									<label for="example-email-input" class="col-form-label"><h5>Laporan Pelaksana Lembur</h5></label>
									
									<div class="form-group row">
										<label for="example-email-input" class="col-2 col-form-label">Rincian Kerja</label>
											<div class="col-10">
												<textarea rows="2" name="rincian" id="rincian" class="form-control"></textarea>
												<div class="invalid-feedback">Silahkan isi rincian kerja</div>
											</div>

										<label for="example-email-input" class="col-2 col-form-label">Hasil Kerja</label>
											<div class="col-10">
												<textarea rows="2" name="hasil" id="hasil" class="form-control"></textarea>
												<div class="invalid-feedback">Silahkan isi hasil kerja</div>
											</div>

										<label for="example-email-input" class="col-2 col-form-label">&nbsp;</label>
											<div class="col-10">
												<button onclick="insert_hasil_kerja()" class="btn btn-info">Simpan</button>
											</div>
									</div>
								</div>
							@else
								<hr>
									<label for="example-email-input" class="col-form-label"><h5>Laporan Pelaksana Lembur</h5></label>
									
									<div class="form-group row">
										<label for="example-email-input" class="col-2 col-form-label">Rincian Kerja</label>
											<div class="col-10">
												<textarea rows="2" name="rincian" id="rincian" class="form-control">{{$data_lembur['rincian_kerja']}}</textarea>
												<div class="invalid-feedback">Silahkan isi rincian kerja</div>
											</div>

										<label for="example-email-input" class="col-2 col-form-label">Hasil Kerja</label>
											<div class="col-10">
												<textarea rows="2" name="hasil" id="hasil" class="form-control">{{$data_lembur['hasil_kerja']}}</textarea>
												<div class="invalid-feedback">Silahkan isi hasil kerja</div>
											</div>

										<label for="example-email-input" class="col-2 col-form-label">&nbsp;</label>
											<div class="col-10">
												<button onclick="insert_hasil_kerja()" class="btn btn-info">Simpan</button>
											</div>
									</div>
							@endif
								
							
							
							
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('lembur.action')
@include('lembur.modal_pengawas')
@include('lembur.modal_petugas')
@endsection
@section('script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
  	function print(id,jns){

		swal.fire({
           title: "Info",
           text: "Setelah dicetak data tidak dapat diubah lagi....teruskan ?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                window.location.href = base_url +'/cetak_lembur?id=' + id +'&jns=' + jns;
            }
        });
		 
        
      } 
  function berita_acara(){

		swal.fire({
           title: "Info",
           text: "Setelah dicetak data tidak dapat diubah lagi....teruskan ?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                $('#ba').show();
            }
        });
		 
        
      } 



  	var Nonbusinessday = ["2020-04-26", "2020-04-27"];
    var Holiday = [];
  $( function() {

  		$.ajax({
			type: 'GET',
			url: base_url + '/libur',
			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				$.each(response, function(i, item) {
					  var date     = new Date(item.tanggal);
					  var year  = date.getFullYear();
				      var month = date.getMonth() + 1;
				      var day   = date.getDate();
				      var yyyymmdd = year +'-'+ month +'-'+ day;
				    
				    Holiday.push(yyyymmdd);
				});
				

			
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

	console.log(Holiday);	
  $('.init-date').datepicker({
  	dateFormat: "dd MM yy",
                beforeShowDay: function (date) {
                    var datestring = jQuery.datepicker.formatDate('yy-m-dd', date);
                    if (Nonbusinessday.indexOf(datestring) != -1) {
                        return [true, "nonbusiness"];
                    } else if (Holiday.indexOf(datestring) != -1) {
                        return [true, "Holidays"];
                    }
                    else {
                        return [true];
                    }                   
                }
            });




  } );
  $('.init-date .Holidays a').tooltip({content:"This is a test"});
  </script>

<script type="text/javascript">

/*
var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                constrainInput: true,
				beforeShowDay: nationalDays
               	 
            });
        }
    };
}();
*/
var KTBootstrapTimepicker = function () {
    return {
        init: function() {
            $('.init-time').timepicker({
	            minuteStep: 1,
	            defaultTime: '',
	            showSeconds: false,
	            showMeridian: false,
	            snapToStep: true
	        });
        }
    };
}();

/*
$(document).ready(function() {
     var SelectedDates = {};     
     SelectedDates[new Date('04/05/2012')] = new Date('04/05/2012');     
     SelectedDates[new Date('05/04/2012')] = new Date('05/04/2012');     
     SelectedDates[new Date('06/06/2012')] = new Date('06/06/2012');      
     var SeletedText = {};     
     SeletedText[new Date('04/05/2012')] = 'Holiday1';     
     SeletedText[new Date('05/04/2012')] = 'Holiday2';     
     SeletedText[new Date('06/06/2012')] = 'Holiday3';        

     $('#txtDate').datepicker({         
     	beforeShowDay: function(date) {             
     	var Highlight = SelectedDates[date];             
     	var HighlighText = SeletedText[date];      

     	   if (Highlight) {        
     	        return [true, "Highlighted", HighlighText]; 
     	    }else {
     	    return [true, '', ''];
     	    }
     	}
     	}); 

     });​ //Code Ends

*/
</script>
@stop