@section('content')
<!--Begin::Section-->
 <style>
        .Holidays a{
            background-color: red !important; 
        }
</style>
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Lembur
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div id="dialog"></div>
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">Tanggal</label>
									<div class="input-group date">
										<input type="text" class="form-control init-date" name="tgl" readonly="" placeholder="Select date" id="tgl">
											<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar-check-o"></i>
											</span>
										</div>
										<div class="invalid-feedback">Silahkan pilih tanggal</div>
									</div>		
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Perkiraan Jam</label>
									<div class="row">
									<div class="col-6">
										<input class="form-control init-time" id="jam_awal" name="jam_awal" readonly="" placeholder="Select time" type="text">	
									</div>
									<div class="col-6">
										<input class="form-control init-time" id="jam_akhir" name="jam_akhir" readonly="" placeholder="Select time" type="text">	
									</div>	
									<div class="invalid-feedback">Silahkan isi jam</div>	
									</div>

								</div>

								<div class="form-group form-group-last">
									<label for="exampleTextarea">Maksud / Tujuan Lembur</label>
									<textarea class="form-control" id="tujuan" name="tujuan" rows="2"></textarea>
									<div class="invalid-feedback">Silahkan isi tujuan</div>
								</div>
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Tempat</label>
									<textarea class="form-control" id="tempat" name="tempat" rows="1"></textarea>
									<div class="invalid-feedback">Silahkan isi tempat</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Pemberi Tugas</label>
									<select class="form-control" id="exampleSelect1" name="user_approval">
										@if($data['data'])
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nmpegawai']}}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="insert()" class="btn btn-primary">Submit</button>
									<button onclick="loadNewPage('{{ route('lembur') }}')" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('lembur.action')
@endsection
@section('script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  	var Nonbusinessday = ["2020-04-26", "2020-04-27"];
    var Holiday = [];
  $( function() {

  		$.ajax({
			type: 'GET',
			url: base_url + '/libur',
			success: function (response) {
				
				var response=JSON.parse(response);
				$("#loading").css('display', 'none');
				$.each(response, function(i, item) {
					  var date     = new Date(item.tanggal);
					  var year  = date.getFullYear();
				      var month = date.getMonth() + 1;
				      var day   = date.getDate();
				      var yyyymmdd = year +'-'+ month +'-'+ day;
				    
				    Holiday.push(yyyymmdd);
				});
				

			
			}

		}).done(function (msg) {
			$("#loading").css('display', 'none');
		}).fail(function (msg) {
			$("#loading").css('display', 'none');
			swal.fire("error",'Terjadi Kesalahan',"error");
            // toastr.error("Terjadi Kesalahan");
        });

	console.log(Holiday);	
  $('.init-date').datepicker({
  	dateFormat: "dd MM yy",
                beforeShowDay: function (date) {
                    var datestring = jQuery.datepicker.formatDate('yy-m-dd', date);
                    if (Nonbusinessday.indexOf(datestring) != -1) {
                        return [true, "nonbusiness"];
                    } else if (Holiday.indexOf(datestring) != -1) {
                        return [true, "Holidays"];
                    }
                    else {
                        return [true];
                    }                   
                }
            });




  } );
  $('.init-date .Holidays a').tooltip({content:"This is a test"});
  </script>

<script type="text/javascript">

/*
var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                constrainInput: true,
				beforeShowDay: nationalDays
               	 
            });
        }
    };
}();
*/
var KTBootstrapTimepicker = function () {
    return {
        init: function() {
            $('.init-time').timepicker({
	            minuteStep: 1,
	            defaultTime: '',
	            showSeconds: false,
	            showMeridian: false,
	            snapToStep: true
	        });
        }
    };
}();

/*
$(document).ready(function() {
     var SelectedDates = {};     
     SelectedDates[new Date('04/05/2012')] = new Date('04/05/2012');     
     SelectedDates[new Date('05/04/2012')] = new Date('05/04/2012');     
     SelectedDates[new Date('06/06/2012')] = new Date('06/06/2012');      
     var SeletedText = {};     
     SeletedText[new Date('04/05/2012')] = 'Holiday1';     
     SeletedText[new Date('05/04/2012')] = 'Holiday2';     
     SeletedText[new Date('06/06/2012')] = 'Holiday3';        

     $('#txtDate').datepicker({         
     	beforeShowDay: function(date) {             
     	var Highlight = SelectedDates[date];             
     	var HighlighText = SeletedText[date];      

     	   if (Highlight) {        
     	        return [true, "Highlighted", HighlighText]; 
     	    }else {
     	    return [true, '', ''];
     	    }
     	}
     	}); 

     });​ //Code Ends

*/
</script>
@stop