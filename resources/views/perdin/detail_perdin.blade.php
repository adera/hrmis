@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Perjalanan Dinas (Perdin)
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<button type="button" onclick="loadNewPage('{{ route('add_perdin') }}')"  class="btn btn-label-info btn-sm btn-upper">Tambah Perdin</button>

									<button type="button" onclick="loadNewPage('{{ route('perdin') }}')"  class="btn btn-danger btn-sm btn-upper">Perdin Sebelumnya</button>
								</div>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}

							<div class="kt-portlet__body">
								<div class="row col-12">
									<div class="col-6">
										<div class="form-group row">
											<label for="example-email-input" class="col-3 col-form-label">NIP</label>
											<div class="col-9">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('nip')}}</label>
											</div>
											<label for="example-email-input" class="col-3 col-form-label">Dari Tanggal</label>
											<div class="col-9">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{date('d M Y',strtotime($data_perdin['tgl_mulai']))}} s.d {{date('d M Y',strtotime($data_perdin['tgl_selesai']))}}</label>
											</div>
											<label for="example-email-input" class="col-3 col-form-label">Uraian</label>
											<div class="col-9">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['uraian']}}</label>
											</div>
											<label for="example-email-input" class="col-3 col-form-label">Jenis Perdin</label>
											<div class="col-9">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['jns_perdin']}}</label>

											</div>
											<label for="example-email-input" class="col-3 col-form-label">Pemberi Tugas</label>
											<div class="col-9">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{Session('pemberi_tugas')}}</label>
												
											</div>

										</div>
									</div>
									<div class="col-6">
										<div class="form-group row">
											<label for="example-email-input" class="col-4 col-form-label">Wilayah</label>
											<div class="col-8">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['wilayah']}}</label>
												
											</div>

											<label for="example-email-input" class="col-4 col-form-label">Provinsi</label>
											<div class="col-8">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['provinsi']}}</label>
											</div>

											<label for="example-email-input" class="col-4 col-form-label">Berangkat Dari</label>
											<div class="col-8">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['asal']}}</label>
											</div>

											<label for="example-email-input" class="col-4 col-form-label">Tujuan</label>
											<div class="col-8">
												<label for="example-email-input" class="col-form-label">: &nbsp;-</label>
											</div>

											<label for="example-email-input" class="col-4 col-form-label">Nama Tempat Tujuan</label>
											<div class="col-8">
												<label for="example-email-input" class="col-form-label">: &nbsp;{{$data_perdin['tujuan']}}</label>
											</div>
		
										</div>
									</div>	
								</div>
								@if($data_perdin['is_print'])
								@else
								<label for="example-email-input" class="col-form-label" style="color: red">Perdin sudah disimpan...</label>
								@endif

								<div class="form-group row">
									<label for="exampleSelect1"><h5>Nomor :</h5></label>
									<div class="input-group date">
										<h4>{{$data_perdin['nomor']}}</h4>
										<hr>
									</div>	
									
									<label for="example-email-input" class="col-1 col-form-label">Tanggal</label>
									<div class="col-11">
										<label for="example-email-input" class="col-form-label">: &nbsp;<b>{{date('d M Y',strtotime($data_perdin['tgl_mulai']))}} s.d {{date('d M Y',strtotime($data_perdin['tgl_selesai']))}} &nbsp; ( {{$data_perdin['durasi']}} Hari )</b></label>
									</div>	
								</div>
								<table class="table table-striped- table-hover" id="table_id" width="100%">
									<thead>
										<tr>
											<th title="Field #1"><font size="1">NIP</font></th>
											<th title="Field #2"><font size="1">Nama Pegawai</font></th>
											<th title="Field #3"><font size="1">Jabatan</font></th>
											<th title="Field #3"><font size="1">Aksi</font></th>
										</tr>
									</thead>
									<tbody>
										@if($data_peserta)
											@foreach($data_peserta as $item)
												<tr>
													<td>{{$item['nip']}}</td>
													<td>{{$item['nmpegawai']}}</td>
													<td>{{$item['jabatan']}}</td>
													<td>
														@if(Session('nip')==$item['nip'])
														@else
														@php
														$id=$item['id'];
														@endphp
														<button type="button" class="btn btn-outline-hover-danger btn-sm btn-icon btn-circle" onclick="hapus_pegawai('{{$id}}');"><i class="fa fa-trash"></i></button>
														@endif
														
													</td>
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
								
								
							</div>
							
							<div class="kt-portlet__foot">
								
								<div class="kt-form__actions" style="text-align: right;">
									<button onclick="cetak('{{$id}}',1)" class="btn btn-info">Print</button>
									<button onclick="cetak('{{$id}}',2)" class="btn btn-info">Print Rincian</button>
									@if($data_perdin['is_print'])
									@else
									<button onclick="add_pegawai()" class="btn btn-danger" data-toggle="modal">Tambah Pegawai</button>
									@endif
									
									
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('perdin.action')
@include('perdin.modal_pegawai')
@endsection
@section('script')

<script>
function goBack() {
  window.history.back();
}
</script>

<script type="text/javascript">
	function cetak(id,jns){

		swal.fire({
           title: "Info",
           text: "Setelah dicetak data tidak dapat diubah lagi....teruskan ?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                window.location.href = base_url +'/cetak_perdin?id=' + id +'&jns=' + jns;
            }
        });
		 
        
      } 
var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                constrainInput: true
               	 
            });
        }
    };
}();	

	$('#wilayah').on('change', function (v) {

		var _items='';
		$.ajax({
	        type: 'GET',
	        url: base_url + '/wilayah/'+this.value,
	        success: function (res) {
	            var data = $.parseJSON(res);
	            _items='<option value="">Silahkan pilih</option>';
	            $.each(data, function (k,v) {
	                _items += "<option value='"+v.id+"'>"+v.nama+"</option>";
	            });

	            $('#province').html(_items);
	        }
	    });


	});
</script>
@stop