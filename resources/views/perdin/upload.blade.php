@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Upload Kwitansi Perdin
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<input type="hidden" name="id" id="id" value="{{$id}}">
								<div class="form-group form-group-last">
									<label for="exampleTextarea">Upload File</label>
									<input type="file" name="file" id="file" class="form-control">
									<div class="invalid-feedback">Silahkan pilih file</div>
								</div>
							</div>
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="upload_kwintansi()" class="btn btn-primary">Submit</button>
									<button onclick="loadNewPage('{{ route('perdin') }}')" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('perdin.action')
@endsection