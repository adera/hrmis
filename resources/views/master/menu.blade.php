
<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
	<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
		<ul class="kt-menu__nav ">
			<li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a onclick="loadNewPage('{{ route('home') }}')" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Dashboard</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
			</li>
			<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Self Service</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
				<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
					<ul class="kt-menu__subnav">
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('absensi') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>

							<span class="kt-menu__link-text">Absensi</span></a>
						</li>
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('cuti') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
							<span class="kt-menu__link-text">Cuti</span></a>
						</li>
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('lembur') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
							<span class="kt-menu__link-text">Lembur</span></a>
						</li>
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('perdin') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
							<span class="kt-menu__link-text">Perdin</span></a>
						</li>
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('ukes') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
							<span class="kt-menu__link-text">Uang Kesehatan</span></a>
						</li>
						@if(session('nrp')=='201100')
						<li class="kt-menu__item " aria-haspopup="true"><a onclick="loadNewPage('{{ route('approval') }}')" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
							<span class="kt-menu__link-text">Approval</span></a>
						</li>
						@endif
						
						

					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>