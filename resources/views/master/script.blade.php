<script>
	var KTAppOptions = {
		"colors": {
			"state": {
				"brand": "#366cf3",
				"light": "#ffffff",
				"dark": "#282a3c",
				"primary": "#5867dd",
				"success": "#34bfa3",
				"info": "#36a3f7",
				"warning": "#ffb822",
				"danger": "#fd3995"
			},
			"base": {
				"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
				"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
			}
		}
	};
</script>

		<!-- end::Global Config -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="{{asset('assets/vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/general/wnumb/wNumb.js')}}" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->
		<script src="{{asset('assets/js/demo1/pages/crud/metronic-datatable/base/html-table.js')}}" type="text/javascript"></script>

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('assets/js/demo4/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/demo1/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->
		<!-- DATA TABLE JS-->
		<script src="{{asset('assets1/plugins/datatable/js/jquery.dataTables.js')}}"></script>
		<script src="{{asset('assets1/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>

		<script src="{{asset('assets1/plugins/datatable/dataTables.responsive.min.js')}}"></script>
		<script src="{{asset('assets1/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
		<script src="{{asset('assets1/plugins/datatable/datatable.js')}}"></script>

		<!-- SELECT2 JS -->
		<script src="{{asset('assets1/js/select2.js')}}"></script>
		<script src="{{asset('assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/bootstrap-timepicker.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets1/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript">
    	</script>
		
		<script src="{{asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="{{asset('assets/js/demo4/pages/dashboard.js ')}}" type="text/javascript"></script>
		<script src="{{asset('js/config.js ')}}" type="text/javascript"></script>


		<script src="{{asset('assets/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>