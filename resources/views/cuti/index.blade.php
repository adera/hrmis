@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head kt-portlet__head--lg">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon">
								<i class="kt-font-brand flaticon2-line-chart"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								Cuti
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								<button type="button" onclick="loadNewPage('{{ route('add_cuti') }}')" class="btn btn-label-info btn-sm btn-upper">Tambah Cuti</button>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">

						<!--begin: Search Form -->
						<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
							<div class="row align-items-center">
								<div class="col-xl-8 order-2 order-xl-1">
									<div class="row align-items-center">
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											<div class="kt-form__group kt-form__group--inline">
												<div class="kt-form__label">
													<label>Bulan:</label>
												</div>
												<div class="kt-form__control">
													<select class="form-control bootstrap-select" id="bulan" name="bulan" id="bulan" data-parsley-required>
														<option value="1" @if($bln==1) selected @endif>Januari</option>
														<option value="2" @if($bln==2) selected @endif>Februari</option>
														<option value="3" @if($bln==3) selected @endif>Maret</option>
														<option value="4" @if($bln==4) selected @endif>April</option>
														<option value="5" @if($bln==5) selected @endif>Mei</option>
														<option value="6" @if($bln==6) selected @endif>Juni</option>
														<option value="7" @if($bln==7) selected @endif>Juli</option>
														<option value="8" @if($bln==8) selected @endif>Agustus</option>
														<option value="9" @if($bln==9) selected @endif>September</option>
														<option value="10" @if($bln==10) selected @endif>Oktober</option>
														<option value="11" @if($bln==11) selected @endif>November</option>
														<option value="12" @if($bln==12) selected @endif>Desember</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											<div class="kt-form__group kt-form__group--inline">
												<div class="kt-form__label">
													<label>Tahun:</label>
												</div>
												<div class="kt-form__control">
													<select class="form-control bootstrap-select" id="tahun">
														@php
														$thn_skr = date('Y');
						                				for ($x = $thn_skr; $x >= 2010; $x--) {
														@endphp
														<option value="{{$x}}" @if($thn==$x) selected @endif>{{$x}}</option>
														@php
														}
														@endphp
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											<div class="kt-form__group kt-form__group--inline">
											<button type="button" onclick="filter();" class="btn btn-info">Filter</button>		
											</div>
										</div>

										
									</div>
								</div>
							</div>
						</div>

						<div class="col-xl-4 order-1 order-xl-2 kt-align-right">
							<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
						</div>
						<table class="table table-striped- table-hover" id="table_id" width="100%">
							<thead>
								<tr>
									<th title="Field #1">Mulai</th>
									<th title="Field #2">Sampai</th>
									<th title="Field #3">Alasan</th>
									<th title="Field #4">Keterangan</th>
								</tr>
							</thead>
							<tbody>
								@if($data['data']['data'])
								@foreach($data['data']['data'] as $item)
								<tr>
									<td>{{date('d M Y',strtotime($item['tgl_mulai']))}}</td>
									<td>{{date('d M Y',strtotime($item['tgl_selesai']))}}</td>
									<td>{{$item['alasan']}}</td>
									<td>
										<div class="kt-widget2">
											<div class="kt-widget2__item kt-widget2__item--danger">
												<div class="kt-widget2__checkbox">
												</div>
												<div class="kt-widget2__info">
													<span class="kt-widget2__title">Tanggal Pengajuan</span>
													<span class="kt-widget2__username">{{date('d M Y',strtotime($item['tgl_pengajuan']))}}</span>
													<a class="kt-widget2__username">
														<span class="btn btn-label-danger btn-sm">{{$item['approval']['nama']}}</span>
														<span class="btn btn-label-success btn-sm">{{$item['approval']['status']}}</span>
														
														@if($item['approval']['status']=='Menunggu Approval')
														<button type="button" onclick="hapus({{$item['id']}});" class="btn btn-danger">Hapus</button>
														@endif
													</a>
												</div>
												<div class="kt-widget2__actions">
											
												</div>
											</div>
										</div>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
							
						</table>
						<!--end: Search Form -->
					</div>
					<div class="kt-portlet__body kt-portlet__body--fit">

						<!--begin: Datatable -->
						

						<!--end: Datatable -->
					</div>
				</div>
		</div>
	</div>
</div>
@include('cuti.action')
@endsection