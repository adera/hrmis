@section('content')
<!--Begin::Section-->
<div class="row">
	<div class="col-xl-12">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="kt-portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Ambil Cuti
								</h3>
							</div>
						</div>

						<!--begin::Form-->
						<form class="kt-form" id="form_kontigensi">
							{{ csrf_field() }}
							<div class="kt-portlet__body">
								<div class="form-group">
									<label for="exampleSelect1">Jenis Cuti</label>
									<select class="form-control" id="exampleSelect1" name="idjns">
										@if($jns['data'])
										@foreach($jns['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nama']}}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
													<label>Jatah Cuti</label>
													<div class="kt-radio-inline">
														@php
														$tahun=date('Y');
														$tahunlalu=$tahun - 1;
														@endphp
														<label class="kt-radio kt-radio--solid kt-radio--brand">
															<input type="radio" name="tahun" value="{{$tahunlalu}}"> Tahun Lalu
															<span></span>
														</label>

														<label class="kt-radio kt-radio--solid kt-radio--brand">
															<input type="radio" name="tahun" value="{{date('Y')}}"> Tahun Ini
															<span></span>
														</label>

													</div>
												<div></div></div>
								<div class="form-group">
									<label for="exampleSelect1">Mulai Tanggal</label>
									<div class="input-daterange input-group" id="kt_datepicker_5">
													<input type="text" class="form-control" id="start" name="start">
													<div class="input-group-append">
														<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
													</div>
													<input type="text" class="form-control" id="end" name="end">
												</div>
									<div class="invalid-feedback">Silahkan pilih tanggal</div>			
								</div>

								<div class="form-group form-group-last">
									<label for="exampleTextarea">Alasan</label>
									<textarea class="form-control" id="alasan" name="alasan" rows="3"></textarea>
									<div class="invalid-feedback">Silahkan isi alasan</div>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Rekomendasi</label>
									<select class="form-control" id="exampleSelect1" name="user_approval1">
										@if($data['data'])
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nmpegawai']}}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Diputuskan</label>
									<select class="form-control" id="exampleSelect1" name="user_approval">
										@if($data['data'])
										@foreach($data['data'] as $item)
										<option value="{{$item['id']}}">{{$item['nmpegawai']}}</option>
										@endforeach
										@endif
									</select>
								</div>
							<div class="form-group">
								<label>Ambil Bekal Cuti</label>
									<div class="kt-radio-inline">
										<label class="kt-radio kt-radio--solid kt-radio--brand">
											<input type="radio" name="ambil_cuti" value="true"> Ya
											<span></span>
										</label>
										<label class="kt-radio kt-radio--solid kt-radio--brand">
										<input type="radio" name="ambil_cuti" value="false"> Tidak
									<span></span>
									</label>
								</div>
							<div></div></div>
							</div>
							
							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button onclick="insert()" class="btn btn-primary">Submit</button>
									<button onclick="loadNewPage('{{ route('cuti') }}')" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</form>

						<!--end::Form-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@include('cuti.action')
@endsection